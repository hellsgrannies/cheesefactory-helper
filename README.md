# cheesefactory-helper

-----------------

##### Contains decorator that provides runtime details in the form of logs, for the parent function.

[comment]: <> ([![PyPI Latest Release]&#40;https://img.shields.io/pypi/v/cheesefactory-logger-sqlite.svg&#41;]&#40;https://pypi.org/project/cheesefactory-logger-sqlite/&#41;)

[comment]: <> ([![PyPI status]&#40;https://img.shields.io/pypi/status/cheesefactory-logger-sqlite.svg&#41;]&#40;https://pypi.python.org/pypi/cheesefactory-logger-sqlite/&#41;)

[comment]: <> ([![PyPI download month]&#40;https://img.shields.io/pypi/dm/cheesefactory-logger-sqlite.svg&#41;]&#40;https://pypi.python.org/pypi/cheesefactory-logger-sqlite/&#41;)

[comment]: <> ([![PyPI download week]&#40;https://img.shields.io/pypi/dw/cheesefactory-logger-sqlite.svg&#41;]&#40;https://pypi.python.org/pypi/cheesefactory-logger-sqlite/&#41;)

[comment]: <> ([![PyPI download day]&#40;https://img.shields.io/pypi/dd/cheesefactory-logger-sqlite.svg&#41;]&#40;https://pypi.python.org/pypi/cheesefactory-logger-sqlite/&#41;)

### Main Features

* Logs function execution time (determined by subtracting finish-time from start-time).
* Logs args and kwargs passed to decorated function. 
* Logs name of decorated function and the name of the file containing the decorated function.



#### Note: This package is still in beta status. As such, future versions may not be backwards compatible and features may change.


[comment]: <> (## Recent Changes)

[comment]: <> (* v0.4: The `CfLogSqlite.result` attribute no longer exists. `CfLogSqlite.read_records&#40;&#41;` now directly )

[comment]: <> (returns a list of query results.)


## Installation
The source is hosted at https://bitbucket.org/hellsgrannies/cheesefactory-helper

```sh
pip install cheesefactory-helper
```

## Dependencies

* logging
* pytest

### Basic Usage

```python
from cheesefactory_helper import runtime_info
import logging

#   initialize a logger
logger = logging.getLogger(__name__)

#   configure the logger settings to show "info" logs or above.
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(module)-20s line:%(lineno)-4d %(levelname)-8s %(message)s'
)

@runtime_info
def addtwo(num1, num2):
    return num1 + num2


def main():
    num1, num2 = 1, 2
    return addtwo(num1=num1, num2=num2)


main()

```

[comment]: <> (* _database_path_ &#40;str&#41;: Path to SQLite database file.)

[comment]: <> (* _create_ &#40;str&#41;: Create a new SQLite database file if it does not exist?)

[comment]: <> (* _field_list_ &#40;dict&#41;: A dictionary of field name "keys" paired with field type "values".)


### Basic Usage Output

```
# This is the runtime_info log output of the above decorated function

2021-03-23 17:20:33,135 __init__             line:19   INFO     Calling function 'addtwo' with arguments (num1=1, num2=2)
2021-03-23 17:20:33,135 __init__             line:21   INFO     Function 'addtwo' Returned Value(s) 3
2021-03-23 17:20:33,135 __init__             line:23   INFO     Function 'addtwo' in File '/opt/project/cheesefactory_helper/addtwo.py' took 0.0002 seconds to run
Process finished with exit code 0
```


#### Note: This package is still in beta status. As such, future versions may not be backwards compatible and features may change.