# test_init.py

from cheesefactory_helper import runtime_info
import pytest
import logging
import re

logger = logging.getLogger(__name__)


@runtime_info
def subtract_two():
    value = 5 - 3
    return value


@runtime_info
def concat_two():
    return 'foo' + 'bar'


@pytest.mark.parametrize(
    'func,result',
    [(subtract_two, 2), (concat_two, 'foobar')]
)
def test_runtime_info(func, result, caplog):
    func_result = func()
    for record in caplog.records:
        assert record.levelname == 'INFO'
    assert func_result == result
    assert re.match('.*Calling.*\n.*Returned.*\n.*seconds to run.*', caplog.text) is not None
    # assert 'Returned Value(s)' in caplog.text
    # assert 'seconds to run' in caplog.text
