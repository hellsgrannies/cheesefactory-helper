# __init__.py

import functools
import time
import inspect
import logging

logger = logging.getLogger(__name__)


def runtime_info(original_function):
    """Prints runtime information for the decorated function """
    @functools.wraps(original_function)
    def wrapper_runtime_info(*args, **kwargs):
        program_start = time.perf_counter()
        arg_rep = [repr(a) for a in args]
        kwarg_rep = [f'{k}={v}' for k, v in kwargs.items()]
        string_rep = ', '.join(arg_rep + kwarg_rep)
        logger.info(f'Calling function {original_function.__name__!r} with arguments ({string_rep})')
        value = original_function(*args, **kwargs)
        logger.info(f'Function {original_function.__name__!r} Returned Value(s) {value}')
        runtime = time.perf_counter() - program_start
        logger.info(f'Function {original_function.__name__!r} in File {inspect.getfile(original_function)!r} '
                    f'took {runtime:.4f} seconds to run')
        return value
    return wrapper_runtime_info
